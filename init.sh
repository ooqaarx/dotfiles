#!/usr/bin/env bash

echo "[+] initialize dotfiles"

for dotfile in dot-*
do
    if [[ ${dotfile} == "dot-ssh_config" ]]; then
        # we have a special case for ssh_config, we won't do anything if the file already exists and has the include
        # statement.
        FILE=$HOME/.ssh/config
        if [[ -f "$FILE" ]]; then
            if ! grep -q "^Include config.d/*" "$FILE"; then
                echo "Include config.d/*" >> ${FILE}
            fi
        else
            # the config file is completely missing so use the default
            FILENAME=$(echo ${dotfile}|sed 's/dot-ssh_//g')
            cp ${dotfile} $HOME/.ssh/${FILENAME}
        fi
    else
        # this is the default case, store under $HOME/.FILENAME
        FILENAME=$(echo ${dotfile}|sed 's/dot-/./g')
        cp ${dotfile} $HOME/${FILENAME}
    fi
done

echo "[+] initializing dotfiles finished"
